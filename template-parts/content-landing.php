<?php
/**
 * Template part for displaying Landing Page
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * Template Name: Landing Page
 * @package leenderhof
 */

 get_header();
 
 if(have_posts()) :
    while (have_posts()) : the_post(); ?>

        <div id="content">
            <div id="landing">
                <div class="section <?php 
                    if( has_post_thumbnail() ){
                        echo 'half-thumbnail';
                    } else {
                        echo 'full-thumbnail no-thumbnail';
                    } ?>">
                    <div class="bg-image"<?php if( has_post_thumbnail() ){ 
                            echo 'style="background-image: url(\'' . get_the_post_thumbnail_url() . '\')"';
                        } ?>></div>
                    <div class="container container-content">
                        <div class="content-set">
                            <?php if(get_field('subtitle_lp')) : ?>
                                <h2 class="title-custom">
                                    <?php the_field('subtitle_lp'); ?>
                                </h2>
                            <?php endif; ?>
                            <h1 class="title">
                                <?php the_title(); ?>
                            </h1>
                        </div>
                    </div>
                </div>

                <div class="section section-the-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-md-8 mx-auto" id="the-content">
                                <?php the_content(); ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="section full-gallery">
                    <?php 
                        if(get_field('gallery_example')) :
                            $image_ids = get_field('gallery_example', false, false);
                            $shortcode = '[' . 'gallery ids="' . implode(',', $image_ids) . '"]';
                            
                            echo do_shortcode( $shortcode );
                        endif;
                    ?>
                </div>
                
                <?php if( get_field('title_lp', 'option') && get_field('prijzen_lp', 'option')) : ?>
                    <div class="section price-button">
                        <div class="container">
                            <div class="wrapper">
                                <div class="content-set white centered">
                                    <h3 class="title-custom">
                                        <?php the_field('subtitle2_lp', 'option'); ?>
                                    </h3>
                                    <h2 class="title">
                                        <?php the_field('title_lp', 'option'); ?>
                                    </h2>
                                    <div class="content">
                                        <?php the_field('content_lp', 'option'); ?>
                                        <a href="<?php the_field('prijzen_lp', 'option'); ?>" class="btn black jumbo">
                                            <?php _e('Prijzen', 'leenderhof'); ?>
                                            <i class="fas fa-arrow-right icon"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>

            </div>
        </div>

<?php 
    endwhile;
endif;

get_footer();