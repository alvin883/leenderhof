<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package leenderhof
 */
?>
        
        <div class="section full-thumbnail <?php 
            if( !has_post_thumbnail() ){
                echo 'no-thumbnail';
            } ?>">
            <div class="bg-image"<?php if( has_post_thumbnail() ){ 
                    echo 'style="background-image: url(\'' .get_the_post_thumbnail_url() . '\')"';
                } ?>></div>
            <div class="container container-content">
                <div class="content-set">
                    <h1 class="title">
                        <?php the_title(); ?>
                    </h1>
                    <div class="title-custom">
                        <?php echo get_post_modified_time("d F Y", null, null, true); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="section section-the-content">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-8 mx-auto" id="the-content">
                        <?php the_content(); ?>
                    </div>
                </div>
            </div>
        </div>
        