<?php
/**
 * Template part for displaying Landing Page
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * Template Name: Price Page
 * @package leenderhof
 */

 get_header();
 
 if(have_posts()) :
    while (have_posts()) : the_post(); ?>

        <div id="content">
            <div id="landing">
                
                <div class="section <?php 
                    if( has_post_thumbnail() ){
                        echo 'half-thumbnail';
                    } else {
                        echo 'full-thumbnail no-thumbnail';
                    } ?>">
                    <div class="bg-image"<?php if( has_post_thumbnail() ){ 
                            echo 'style="background-image: url(\'' .get_the_post_thumbnail_url() . '\')"';
                        } ?>></div>
                    <div class="container container-content">
                        <div class="content-set">
                            <?php if( get_field('subtitle_prices','option') ) : ?>
                                <h2 class="title-custom">
                                    <?php the_field('subtitle_prices','option'); ?>
                                </h2>
                            <?php endif; ?>
                            <h1 class="title">
                                <?php the_title(); ?>
                            </h1>
                        </div>
                    </div>
                </div>

                <div class="section section-the-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-md-8 mx-auto" id="the-content">
                                <?php the_content(); ?>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                <div class="section price-box">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <div class="item">
                                    <?php if(get_field('image_vprice','option')) : ?>
                                        <?php $image = get_field('image_vprice','option'); ?>
                                        <div class="photo" style="background-image: url(<?php echo $image['url']; ?>);">
                                            <div class="content-set centered black">
                                                <?php if(get_field('sub_vm', 'option')) : ?>
                                                    <div class="title-custom">
                                                        <?php the_field('sub_vm', 'option'); ?>
                                                    </div>
                                                <?php endif; ?>
                                                <div class="title">
                                                    <?php the_field('title_vrouwen', 'option'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>

                                    
                                    <div class="prices">
                                        <?php if(have_rows('prijzen_vrouwen', 'option')): 
                                            while(have_rows('prijzen_vrouwen', 'option')): the_row(); ?>
                                                <div>
                                                    <div class="service-name">
                                                        <?php the_sub_field('name_vrouwen', 'option'); ?>
                                                    </div>
                                                    <div class="price">
                                                        <?php the_sub_field('price_vrouwen', 'option'); ?>
                                                    </div>
                                                </div>
                                            <?php endwhile; ?>
                                        <?php endif; ?>
                                     </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="item">
                                    <?php if(get_field('image_vprice','option')) : ?>
                                        <?php $image = get_field('image_mprice','option'); ?>
                                        <div class="photo" style="background-image: url(<?php echo $image['url']; ?>);">
                                            <div class="content-set centered black">
                                                <?php if(get_field('sub_vm', 'option')) : ?>
                                                    <div class="title-custom">
                                                        <?php the_field('sub_vm', 'option'); ?>
                                                    </div>
                                                <?php endif;?> 
                                                <div class="title">
                                                    <?php the_field('title_mannen', 'option'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>

                                    <div class="prices">
                                        <?php if(have_rows('prijzen_mannen', 'option')): 
                                            while(have_rows('prijzen_mannen', 'option')): the_row(); ?>
                                                <div>
                                                    <div class="service-name">
                                                        <?php the_sub_field('name_mannen', 'option'); ?>
                                                    </div>
                                                    <div class="price">
                                                        <?php the_sub_field('price_mannen', 'option'); ?>
                                                    </div>
                                                </div>
                                            <?php endwhile; ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <?php if( get_field('title_prices','option') && get_field('prijzen_prices','option')) : ?>
                    <div class="section price-button">
                        <div class="container">
                            <div class="wrapper">
                                <div class="content-set white centered">
                                <?php if(get_field('subtitle2_prices','option')) : ?>
                                    <h3 class="title-custom">
                                        <?php the_field('subtitle2_prices','option'); ?>
                                    </h3>
                                <?php endif; ?>
                                    <h2 class="title">
                                        <?php the_field('title_prices','option'); ?>
                                    </h2>
                                    <div class="content">
                                        <?php the_field('content_prices', 'option'); ?>
                                        <a href="<?php the_field('prijzen_prices','option'); ?>" class="btn black jumbo">
                                            <?php _e('Contact us', 'leenderhof'); ?>
                                            <i class="fas fa-arrow-right icon"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>

            </div>
        </div>

<?php 
    endwhile;
endif;

get_footer();