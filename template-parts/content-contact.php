<?php
/**
 * Template part for displaying Contact Page
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * Template Name: Contact Page
 * @package leenderhof
 */

 get_header();
 
 if(have_posts()) :
    while (have_posts()) : the_post(); ?>

        <div id="content">
            <div id="contact">
                
                <div class="section full-thumbnail <?php if( !has_post_thumbnail() ){ echo 'no-thumbnail'; } ?>">
                    <div class="bg-image" <?php if( has_post_thumbnail() ){ 
                            echo 'style="background-image: url(\'' . get_the_post_thumbnail_url() . '\')"';
                        } ?>></div>
                    <div class="container container-content">
                        <div class="content-set">
                            <?php if(get_field('subtitle_contact')) :?>
                                <h2 class="title-custom">
                                    <?php the_field('subtitle_contact'); ?>
                                </h2>
                            <?php endif; ?>
                            <h1 class="title">
                                <?php the_title(); ?>
                            </h1>
                        </div>
                    </div>
                </div>

                <div class="section section-the-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-md-8 mx-auto" id="the-content">
                                <?php the_content(); ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="section section-contact">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-md-5">
                                <div class="item">
                                    <div class="content-set">
                                        <div class="title">
                                            <?php _e('Openingstijden', 'leenderhof'); ?>
                                        </div>
                                        <div class="content">
                                            <?php if (have_rows('daily_time', 'option')) :
                                                while (have_rows('daily_time', 'option')) : the_row(); ?>
                                                    <div>
                                                        <?php
                                                            the_sub_field('days', 'option');
                                                            echo " : <span>" . get_sub_field('open_daily', 'option') . "</span>";
                                                        ?>
                                                    </div>
                                                <?php endwhile; 
                                            endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-7">
                                <div class="row contact">
                                    <div class="col-12 col-lg-6">
                                        <a href="mailto:<?php the_field('email', 'option'); ?>">
                                            <div class="item">
                                                <div>
                                                    <i class="fas fa-envelope icon"></i>
                                                </div>
                                                <div>
                                                    <?php the_field('email', 'option'); ?>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-12 col-lg-6">
                                        <a href="tel:<?php echo str_replace(' ', '', str_replace('-', '', get_field('telp', 'option'))); ?>">
                                            <div class="item">
                                                <div>
                                                    <i class="fas fa-phone icon"></i>
                                                </div>
                                                <div>
                                                    <?php the_field('telp', 'option'); ?>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                        <div class="col-12 col-lg-6">
                                            <a href="<?php the_field('soc_med_fb_link', 'option'); ?>" target="_blank">
                                                <div class="item">
                                                    <div>
                                                        <i class="fab fa-facebook-f icon"></i>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <?php if(get_field('title_maps')) : ?>
                    <div class="section full-gmaps">
                        <div class="container">
                            <div class="content-set centered">
                                <?php if(get_field('subtitle2_maps')) : ?>
                                    <h3 class="title-custom">
                                        <?php the_field('subtitle2_maps'); ?>
                                    </h3>
                                <?php endif;?>
                                <h2 class="title">
                                    <?php the_field('title_maps'); ?>
                                </h2>
                            </div>
                        </div>
                        <div class="wrapper">
                        <?php the_field('embed_iframe', 'option'); ?>
                        </div>
                    </div>
                <?php endif; ?>

            </div>
        </div>

<?php 
    endwhile;
endif;

get_footer();