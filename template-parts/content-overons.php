<?php
/**
 * Template part for displaying Overons Page
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * Template Name: Overons Page
 * @package leenderhof
 */

 get_header();
 
 if(have_posts()) :
    while (have_posts()) : the_post(); ?>

        <div id="content">
            <div id="overons">
                
                <div class="section full-thumbnail">
                    <div class="bg-image" <?php if( has_post_thumbnail() ){ 
                            echo 'style="background-image: url(\'' . get_the_post_thumbnail_url() . '\')"';
                        } ?>></div>
                    <div class="container container-content">
                        <div class="content-set">
                            <?php if(get_field('subtitle_overons')) : ?>
                                <h2 class="title-custom">
                                    <?php the_field('subtitle_overons'); ?>
                                </h2>
                            <?php endif; ?>
                            <h1 class="title">
                                <?php the_title(); ?>
                            </h1>
                        </div>
                    </div>
                </div>

                <div class="section section-the-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-md-8 mx-auto" id="the-content">
                                <p><?php the_content(); ?></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="section section-3">
                    <div class="container">
                        <div class="row">
                            <?php if( get_field('services_point') ) : ?>
                                <div class="col-12 col-lg-6">
                                    <div class="row category-list">
                                        <?php  $posts = get_field('services_point'); 
                                        if($posts) : ?>
                                            <?php foreach ($posts as $pst): ?>
                                                <div class="col-12 col-md-6 mx-auto" >
                                                    <a href="<?php the_permalink($pst->ID); ?>">
                                                        <div style="background-image: url(<?php echo get_the_post_thumbnail_url($pst->ID); ?>);">
                                                            <div class="wrapper">
                                                                <?php echo get_the_title( $pst->ID ); ?>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            <?php endif; ?>

                            <?php if( get_field('flirting_words') && get_field('content_overons') ) : ?>
                                <div class="col-12 col-lg-6">
                                    <div class="content-set">
                                        <h4 class="title-custom">
                                            <?php the_field('flirting_words'); ?>
                                        </h4>
                                        <h3 class="title">
                                            <?php the_field('title_overons'); ?>
                                        </h3>
                                        <p class="content">
                                            <?php the_field('content_overons'); ?>   
                                        </p>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div><!--.row-->
                    </div>
                </div>

            </div>
        </div>

<?php 
    endwhile;
endif;

get_footer();