<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package leenderhof
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

	<nav class="navbar navbar-expand-xl fixed-top" id="navbar">
		<div class="top">
			<div class="container">
				<div class="left">
					<i class="icon fas fa-map-marker-alt"></i>
					<?php the_field('adres', 'option'); ?>
				</div>
				<div class="right">
					<a href="tel:<?php the_field('telp', 'option'); ?>">
						<i class="icon fas fa-phone"></i>
						<span class="hide">
							<?php the_field('telp', 'option'); ?>
						</span>
					</a>
				</div>
			</div>
		</div>
		<div class="bottom">
			<div class="container">
				<div class="left-side">
					<?php if (function_exists('the_custom_logo')) {
							the_custom_logo();
					}?>
				</div>
				
				<?php #only appear when screen <= `lg` size ?>
				<div class="right-side-collapse">
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
						<i class="fas fa-bars"></i>
					</button>
					
					<div class="divider facebook-small"></div>
					<a href="https://facebook.com" target="_blank" class="facebook-button facebook-small">
						<i class="fab fa-facebook-f"></i>
					</a>
				</div>

				<div class="right-side navbar-collapse collapse" id="navbarContent">
					<?php
						$args = array(
							'theme_location' => 'primary',
							'depth'      => 2,
							'container'  => false,
							'menu_class'     => 'navbar-nav',
							'walker'     => new Bootstrap_Walker_Nav_Menu()
							);
						if (has_nav_menu('primary')) {
							wp_nav_menu($args);
						}
					?>
				</div>
				
				<?php #only appear when screen > `lg` size ?>
				<div class="divider facebook-large"></div>
				<?php if( get_field('soc_med_fb_link', 'option')) : ?>
					<a href="<?php the_field('soc_med_fb_link', 'option'); ?>" target="_blank" class="facebook-button facebook-large">
						<i class="fab fa-facebook-f"></i>
					</a>
				<?php endif; ?>

			</div>
		</div>
	</nav>