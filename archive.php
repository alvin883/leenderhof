<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package leenderhof
 */

get_header(); ?>

    <div id="content">

            <div class="section full-thumbnail">
                <div class="bg-image" <?php if( has_post_thumbnail() ){ 
                        echo 'style="background-image: url(\'' . get_the_post_thumbnail_url() . '\')"';
                    } ?>></div>
                <div class="container container-content">
                    <div class="content-set">
                        
                        <h2 class="title-custom">
                            <?php _e('Archive page', 'leenderhof'); ?>
                        </h2>

                        <?php if(is_year()) { ?>
                            <h1 class="title">
                                <?php _e('Year ', 'leenderhof'); the_time('Y'); ?>
                            </h1> 
                        <?php } else if(is_month()) { ?>
                            <h1 class="title">
                                <?php the_time('F Y'); ?>
                            </h1>
                        <?php } else if(is_date()) { ?>
                            <h1 class="title">
                                <?php the_time('d F Y'); ?>
                            </h1>
                        <?php } ?>

                        <div class="content">
                            <?php echo 'Found ' . $GLOBALS['wp_query']->post_count . ' post'; ?>
                        </div>

                    </div>
                </div>
            </div>

            <div class="section box-list with-shadow">
                <div class="container">
                    <div class="row">

                        <?php 
                            if ( have_posts() ) :
                            while ( have_posts() ) : the_post(); ?>
                                <div class="col-12 col-md-6 col-lg-4">
                                    <a href="<?php the_permalink(); ?>">
                                        <div class="item" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);">
                                            <div class="content-set">
                                                <h5 class="title-custom">
                                                    <?php the_time('F j, Y'); ?>
                                                </h5>
                                                <h3 class="title">
                                                    <?php the_title(); ?>
                                                </h3>
                                                <div class="content">
                                                    <?php echo content(40); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                            <?php endwhile;
                        endif; ?>

                    </div><!--.row-->
                </div><!--.container-->
            </div><!--.section-2-->


            <!-- Bigger than 500 px screen -->
            <div class="post-navigation wide">
                <div class="info">
                    <?php 
                        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                        echo "Page " . $paged . " of " . $wp_query->max_num_pages;
                    ?>
                </div>
                <div class="all-page-number">
                    <?php 
                        echo paginate_links(array(
                            'total' => $wp_query->max_num_pages
                        ));
                    ?>
                </div>
            </div>

            <!-- Smaller than 500 px screen -->
            <div class="post-navigation mobile">
                <div class="info">
                    <?php 
                        echo "Page " . $paged . " of " . $wp_query->max_num_pages;
                    ?>
                </div>
                <div class="all-page-number">
                    <?php
                        previous_posts_link('&laquo; Previous');
                        next_posts_link('Next &raquo;'); 
                    ?>
                </div>
            </div>
            
    </div>

<?php get_footer(); ?>
