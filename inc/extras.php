<?php
/**
 * Custom functions that act independently of the theme templates
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package leenderhof
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function leenderhof_body_classes( $classes ) {
	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'leenderhof_body_classes' );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function leenderhof_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
	}
}
add_action( 'wp_head', 'leenderhof_pingback_header' );


/**
 * Add Bootstrap button classes to tag cloud
 */
function leenderhof_tag_cloud_btn( $return ) {
	$return = str_replace('<a', '<a class="btn btn-secondary btn-sm"', $return );
	return $return;
}
add_filter( 'wp_tag_cloud', 'leenderhof_tag_cloud_btn' );


/**
 * Customize the Read More Button
**/
function leenderhof_modify_read_more_link() {
    return sprintf( '<a class="more-link btn btn-sm btn-secondary" href="%1$s">%2$s</a>',
        get_permalink(),
        __( 'Read More', 'leenderhof' )
    );
}
add_filter( 'the_content_more_link', 'leenderhof_modify_read_more_link' );

/*
* Plain Menu output
* No <ul>, no <li>, just <a>
**/
function plain_menu($location) {
  $menuLocations = get_nav_menu_locations(); // This returns an array of menu locations;

  $menuID = $menuLocations[$location]; // Get the *MENU* menu ID

  $menu_navs = wp_get_nav_menu_items($menuID);

  $queried_page_id = get_queried_object_id();
  ?>
      <?php
      foreach ( $menu_navs as $menu_nav ) {

        $object_id = intval($menu_nav->object_id);

        if ( $queried_page_id == $object_id ) {
          $active = " class='active' ";
        } else {
          $active = '';
        }

        echo '<a href="'. esc_url( $menu_nav->url ) .'" '. $active .' title="'. esc_html( $menu_nav->title ) .'">'. esc_html( $menu_nav->title ) .'</a>';
      }
      ?>
    </ul><!-- close UL -->
  <?php 
}

/* The content Limitation*/ 
function content($limit) {
  $content = explode(' ', get_the_content(), $limit);
  if (count($content)>=$limit) { 
    array_pop($content);
    $content = implode(" ",$content).' ...';
  } else {
    $content = implode(" ",$content);
  } 
  $content = preg_replace('/\[.+\]/','', $content);
  $content = apply_filters('the_content', $content); 
  $content = str_replace(']]>', ']]&gt;', $content);
  $content = strip_tags($content);
  return $content;
}

/* Function ACF */
if (function_exists('acf_add_options_page')) {
	acf_add_options_page(array(
    'page_title'  => 'Theme General Settings',
    'menu_title'  => 'Theme Settings',
    'menu_slug'   => 'theme-settings',
    'capability'  => 'edit_posts',
    'redirect'    => false
  ));

  acf_add_options_sub_page(array(
    'page_title'  => 'Homepage Settings',
    'menu_title'  => 'Homepage Setting',
    'parent_slug' => 'theme-settings',
  ));
  
  acf_add_options_sub_page(array(
    'page_title'  => 'Prijzen Settings',
    'menu_title'  => 'Prijzen Setting',
    'parent_slug' => 'theme-settings',
  )); 

}

/* Post Pagination */
add_filter('next_posts_link_attributes', 'next_posts_link_attributes');
add_filter('previous_posts_link_attributes', 'prev_posts_link_attributes');

function next_posts_link_attributes(){
  return 'class="next page-numbers"';
} 

function prev_posts_link_attributes(){
  return 'class="prev page-numbers"';
}

/** Function To display Child Page */
function wpb_list_child_pages() { 
  global $post; 
   
  if ( is_page() && $post->post_parent )
      $childpages = wp_list_pages( 'sort_column=menu_order&title_li=&child_of=' . $post->post_parent . '&echo=0' );
  else
      $childpages = wp_list_pages( 'sort_column=menu_order&title_li=&child_of=' . $post->ID . '&echo=0' );
  if ( $childpages ) {
      $string = '<ul>' . $childpages . '</ul>';
  }
  return $string;
  }
  add_shortcode('wpb_childpages', 'wpb_list_child_pages');

