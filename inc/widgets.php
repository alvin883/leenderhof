<?php
/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function jcm_widgets_init() {
	register_sidebar( array(
        'name' => __( 'footer1', 'leenderhof' ),
        'id' => 'sidebar1',
        'description' => __( 'The contact data', 'jcmbest' ),
        'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
        'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
        'after_widget' => '</div>',
    ) );
}
add_action( 'widgets_init', 'jcm_widgets_init' );
