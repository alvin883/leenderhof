<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package leenderhof
 */
get_header();

if(have_posts()) :
   while (have_posts()) : the_post(); ?>

       <div id="content">
            <div class="section full-thumbnail <?php if( !has_post_thumbnail() ){ echo 'no-thumbnail'; } ?>">
                <div class="bg-image" <?php if( has_post_thumbnail() ){ 
                        echo 'style="background-image: url(\'' . get_the_post_thumbnail_url() . '\')"';
                    } ?>></div>
                <div class="container container-content">
                    <div class="content-set">
                        <h3 class="title-custom">
                            <?php the_time('d F Y'); ?>
                        </h3>
                        <h2 class="title">
                            <?php the_title(); ?>
                        </h2>
                    </div>
                </div>
            </div>

            <div class="section section-the-content">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-md-8 mx-auto" id="the-content">
                            <?php the_content(); ?>
                        </div>
                    </div>
                </div>
            </div>

            <?php if( get_field('title_lp', 'option') && get_field('prijzen_lp', 'option')) : ?>
                <div class="section price-button">
                    <div class="container">
                        <div class="wrapper">
                            <div class="content-set white centered">
                                <h3 class="title-custom">
                                    <?php the_field('subtitle2_lp', 'option'); ?>
                                </h3>
                                <h2 class="title">
                                    <?php the_field('title_lp', 'option'); ?>
                                </h2>
                                <div class="content">
                                    <?php the_field('content_lp', 'option'); ?>
                                    <a href="<?php the_field('prijzen_lp', 'option'); ?>" class="btn black jumbo">
                                        <?php _e('Prijzen', 'leenderhof'); ?>
                                        <i class="fas fa-arrow-right icon"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>

       </div>

<?php 
   endwhile;
endif;

get_footer();
