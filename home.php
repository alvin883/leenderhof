<?php
/**
 * Template part for displaying Blog List
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package leenderhof
 */

get_header(); ?>

    <div id="content">

            <div class="section full-thumbnail">
                <div class="bg-image" <?php if( has_post_thumbnail() ){ 
                        echo 'style="background-image: url(\'' . get_the_post_thumbnail_url() . '\')"';
                    } ?>></div>
                <div class="container container-content">
                    <div class="content-set">
                        <h1 class="title">
                            <?php single_post_title(); ?>
                        </h1>
                    </div>
                </div>
            </div>

            <div class="section box-list with-shadow">
                <div class="container">
                    <div class="row">
                        <?php if ( have_posts() ) :
                            while ( have_posts() ) : the_post(); ?>
                                <div class="col-12 col-md-6 col-lg-4">
                                    <a href="<?php the_permalink(); ?>">
                                        <div class="item" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);">
                                            <div class="content-set">
                                                <h2 class="title">
                                                    <?php the_title(); ?>
                                                </h2>
                                                <div class="subtitle">
                                                    <h5><?php the_time('F j, Y'); ?></h5>
                                                </div>
                                                <div class="content">
                                                    <?php echo content(40); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                            <?php endwhile;
                        endif; ?>
                    </div><!--.row-->
                </div><!--.container-->
            </div><!--.section-2-->

            <!-- Bigger than 500 px screen -->
            <div class="post-navigation wide">
                <div class="info">
                    <?php 
                        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                        echo "Page " . $paged . " of " . $wp_query->max_num_pages;
                    ?>
                </div>
                <div class="all-page-number">
                    <?php 
                        echo paginate_links(array(
                            'total' => $wp_query->max_num_pages
                        ));
                    ?>
                </div>
            </div>

            <!-- Smaller than 500 px screen -->
            <div class="post-navigation mobile">
                <div class="info">
                    <?php 
                        echo "Page " . $paged . " of " . $wp_query->max_num_pages;
                    ?>
                </div>
                <div class="all-page-number">
                    <?php
                        previous_posts_link('&laquo; Previous');
                        next_posts_link('Next &raquo;'); 
                    ?>
                </div>
            </div>

    </div>

<?php 
    get_footer();
?>