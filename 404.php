<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package jcm
 */

get_header(); ?>

	<div id="content">
        <div id="page-404">
			<div class="section section-1">
				<div class="container">
					<div class="content-set">
						<iframe src="https://giphy.com/embed/KDRv3QggAjyo" width="480" height="371" frameBorder="0" class="giphy-embed" allowFullScreen></iframe>
						<h3 class="title-custom">404 Error !</h3>
						<h1 class="title">Not found :(</h1>
						<div class="content">
							Sorry, we can't find the page you are looking for, you entering empty state .
							<a href="<?php echo get_home_url(); ?>" class="btn">
								Go to Homepage
								<i class="fas fa-arrow-right"></i>
							</a>
						</div>
					</div>
				</div>
			</div>
        </div><!-- #page-404 -->
	</div><!-- #content-->
	
<?php
get_footer();
