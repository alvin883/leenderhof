<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package leenderhof
 */

?>
<footer id="footer">

	<?php if(!is_page('contact')) : ?>

		<div class="top">
			<div class="bg-image" style="background-image: url(<?php echo get_stylesheet_directory_uri() . "/src/img/footer-compressed.jpg"; ?>);"></div>
			<div class="container">
				<div class="row">
					<div class="col-12 col-lg-7">
						<div class="content-set">
							<div class="title-custom">
								<?php _e('Afspraak Maken', 'leenderhof'); ?>
							</div>
							<div class="title">
								<?php _e('Contact informatie', 'leenderhof'); ?>
							</div>
							<div class="content">
								<div class="row">
									<div class="col-12 col-md-6">
										<div>
											<?php _e('U kunt ons vinden op :', 'leenderhof'); ?>
										</div>
										<div>
											<span>
												<?php the_field('adres', 'option'); ?>
											</span>
										</div>
										<br/>
										<div>
											<?php _e('Telefoon : ', 'leenderhof'); ?>
											<a href="tel:<?php echo str_replace(' ', '', str_replace('-', '', get_field('telp', 'option'))); ?>">
												<?php the_field('telp', 'option'); ?>
											</a>
										</div>
										<div>
											<?php _e('Email : ', 'leenderhof'); ?>
											<a href="mailto:<?php the_field('email', 'option'); ?>">
												<?php the_field('email', 'option'); ?>
											</a>
										</div>
			  	  		 			</div>               
									<div class="col-12 col-md-6 schedule">
										<?php if (have_rows('daily_time', 'option')) :
											while (have_rows('daily_time', 'option')) : the_row(); ?>
												<div>
													<?php
														the_sub_field('days', 'option');
														echo " : <span>" . get_sub_field('open_daily', 'option') . "</span>";
													?>
												</div>
											<?php endwhile; 
										endif; ?>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-12 col-lg-5">
						<?php the_field('embed_iframe', 'option'); ?>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>		

	<div class="bottom">
		<div class="container">
			<div class="left">
				&copy; Kapsalon Leenderhof <?php if (has_nav_menu('footer')) { 
						plain_menu('footer'); } ?>
			</div>
			<div class="right">
				Designed by 
				<a href="https://www.wappstars.nl/" target="_blank">Wappstars BV</a>
			</div>
		</div>
	</div>
		
</footer>
<?php 
	wp_footer();
?>
</body>
</html>

