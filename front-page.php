<?php
/**
 * @package jcm
 * @subpackage theme name here
 * 	
 */
?>

<?php get_header(); ?>

<div id="homepage">
    <div id="content">

        <div class="section section-1">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-8 bg-image" style="background-image: url(<?php echo get_stylesheet_directory_uri() . "/src/img/section-1.png"; ?>);"></div>
                    <?php if (get_field('title_sec1', 'option')): ?>
                        <div class="col-12 col-lg-4">
                            <div class="content-set white">
                                <h3 class="title-custom">
                                    <?php the_field('intro_sec1', 'option'); ?>
                                </h3>
                                <h1 class="title">
                                    <?php the_field('title_sec1', 'option'); ?>
                                </h1>
                                <p class="content">
                                    <?php the_field('content_sec1', 'option'); ?>
                                </p>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div> 

        <?php if(get_field('title_sec2', 'option') && get_field('content_sec2', 'option')) : ?>
            <div class="section half-thumbnail">
                <div class="bg-image" style="background-image: url(<?php echo get_stylesheet_directory_uri() . "/src/img/section-2.png"; ?>);"></div>
                <div class="container container-content">
                    <div class="col-12 col-lg-8">
                        <div class="content-set">
                            <?php if( get_field('subtitle_sec2', 'option') ): ?>
                                <h3 class="title-custom">
                                    <?php the_field('subtitle_sec2', 'option'); ?>
                                </h3>
                            <?php endif; ?>
                            <h2 class="title">
                                <?php the_field('title_sec2', 'option'); ?>
                            </h2>
                            <div class="content">
                                <?php the_field('content_sec2', 'option'); ?>
                                <a href="<?php the_field('button_sec2', 'option'); ?>" class="btn"><?php _e('verder lezen', 'leenderhof'); ?></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <?php if(get_field('image_vrouwen', 'option') && get_field('image_mannen', 'option')) : ?>                  
            <div class="section section-3">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-lg-6">
                            <a href="<?php the_field('link_vrouwen', 'option'); ?>">
                                <?php $image = get_field('image_vrouwen', 'option'); ?>
                                <div class="item" style="background-image: url(<?php echo $image['url']; ?>);">
                                    <div class="text">
                                        <h3 class="subtitle">
                                            <?php the_field('sub_gender', 'option'); ?>
                                        </h3>
                                        <div class="title">
                                            <?php the_field('title_vrouwen', 'option'); ?>
                                        </div>
                                    </div>
                                </div> 
                            </a>
                        </div>
                        <div class="col-12 col-lg-6">
                            <a href="<?php the_field('link_mannen', 'option'); ?>">
                                <?php $image = get_field('image_mannen', 'option'); ?>
                                <div class="item" style="background-image: url(<?php echo $image['url']; ?>);">
                                    <div class="text">
                                        <h3 class="subtitle">
                                            <?php the_field('sub_gender', 'option'); ?>
                                        </h3>
                                        <div class="title">
                                            <?php the_field('title_mannen', 'option'); ?>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <div class="section section-4">
            <div class="bg-section" style="background-image: url(<?php echo get_stylesheet_directory_uri() . "/src/img/nice_girl.png"; ?>);"></div>
            <?php if( get_field('link_to_sec4', 'option')) : ?>
                <div class="container">
                    <div class="row">
                        <div class="wrapper col-12 col-md-6">
                            <div class="bg-image" style="background-image: url(<?php echo get_stylesheet_directory_uri() . "/src/img/section-4-box-cropped.jpg"; ?>);"></div>
                            <div class="item">
                                <div class="row">
                                    <?php  $posts = get_field('link_to_sec4', 'option'); ?>
                                    <?php if($posts) : ?>
                                        <?php foreach ($posts as $pst): ?>
                                            <div class="col-6">
                                                <a href="<?php echo get_permalink( $pst->ID ); ?>"><?php echo get_the_title( $pst->ID ); ?></a>
                                            </div>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </div>
                                <a href="<?php the_field('link_sec4','option'); ?>" class="btn black"><?php _e('alle diensten', 'leenderhof'); ?></a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>

    </div>
 </div>

<?php 
get_footer();
