jQuery(document).ready(function($){
    // Target URL (#blablabla) onload
    if( $(window.location.hash).length ){
        $('html,body').animate({
            scrollTop: $(window.location.hash).offset().top
        }, 500, 'swing');
    }
    
    // Target URL (#blablabla) onclick *In same location
    $('a[href^="#"]').click(function(e){
        e.preventDefault();
        $('html,body').animate({
            scrollTop: $($(this).attr('href')).offset().top
        }, 500, 'swing');
        if( $(this).parents(".navbar-nav") && $(this).parents("nav") ){
            $($(this).parents()[1]).find("li.active").each(function(){ // Find current .active element
                $(this).removeClass("active");
            });
            $(this).parent().addClass("active");
        }
    });
    
    // Navbar, Toggle small navigation when user scroll
    function checkNavbar(){
        var scroll = $(window).scrollTop();
        if(scroll > 100){
            $("#navbar").addClass("white");
        }else{
            $("#navbar").removeClass("white");
        }
    }
    // check onReady
    checkNavbar();
    
    $(window).scroll(function(){
        var scroll = $(window).scrollTop();
        // check onScroll
        checkNavbar();
    });

    /** Swipebox */
    $( '.gallery-native,.wp-block-gallery' ).each(function(i){
        var newClass= 'gallery_'+ i;
        $(this).addClass(newClass);
        $('.' + newClass + ' a').swipebox({
            useCSS : true, // false will force the use of jQuery for animations
            useSVG : false, 
        });
    });
});